public class Person {
    private String name;
    private String lastName;
    private String username;
    private String password;
    private StringCryption stringCryption;
    Person(String name, String lastName, String username, String password){
        this.name = name;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        stringCryption = new StringCryption(10);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return stringCryption.encrypt(password);
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean login(String username, String password){
        return this.username.equals(username) && this.password.equals(stringCryption.decrypt(password));
    }
}
