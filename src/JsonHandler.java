import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

public class JsonHandler {
    public User readUserData(String username){
        JSONParser jsonParser = new JSONParser();
        User user = null;
        try(FileReader reader = new FileReader("/tmp/" + username + ".json")){
            Object object = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) object;
            user = new User(jsonObject.get("name").toString(), jsonObject.get("lastName").toString(), jsonObject.get("username").toString(), jsonObject.get("password").toString());
        } catch (IOException | ParseException e){
            e.printStackTrace();
        }
        return user;
    }
    public void writeUserData(User user){
        JSONObject employeeDetails = new JSONObject();
        employeeDetails.put("name", user.getName());
        employeeDetails.put("lastName", user.getLastName());
        employeeDetails.put("username", user.getUsername());
        employeeDetails.put("password", user.getPassword());
        try(FileWriter file = new FileWriter("/tmp/" + user.getUsername() + ".json")) {
            file.write(employeeDetails.toJSONString());
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public String getJsonString(User user){
        JSONObject employeeDetails = new JSONObject();
        employeeDetails.put("name", user.getName());
        employeeDetails.put("lastName", user.getLastName());
        employeeDetails.put("username", user.getUsername());
        employeeDetails.put("password", user.getPassword());
        return employeeDetails.toJSONString();
    }
}
